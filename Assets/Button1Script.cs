﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Button1Script : MonoBehaviour {

    public Button button1;
    public GameObject panel1;
    public Text myText;

    void Start()
    {
        Button myButton = button1.GetComponent<Button>();
        myButton.onClick.AddListener(TaskOnClick);
        myText = panel1.GetComponentInChildren<Text>();
    }

    void TaskOnClick()
    {
        myText.text = "Data";
    }
}
