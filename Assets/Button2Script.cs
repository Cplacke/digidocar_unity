﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Button2Script : MonoBehaviour
{
    public Button button1;
    public GameObject panel1;
    public Text myText;

    void Start()
    {
        Button myButton = button1.GetComponent<Button>(); //assign button
        myButton.onClick.AddListener(TaskOnClick); //make button click an actual method
        myText = panel1.GetComponentInChildren<Text>(); // assign text from panel's children
    }

    void TaskOnClick()
    {
        myText.text = "Screenshot"; //change text on button click
    }

}
