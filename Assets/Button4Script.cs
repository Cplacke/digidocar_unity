﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Button4Script : MonoBehaviour
{

    public Button button1;
    public GameObject imageTarget1;
    public TextMesh myText;
    public string a;

    void Start()
    {
        Button myButton = button1.GetComponent<Button>();
        myButton.onClick.AddListener(TaskOnClick);
        myText = imageTarget1.GetComponentInChildren<TextMesh>();
        a = myText.text;
        myText.text = "";
    }

    void TaskOnClick()
    {
        if (myText.text == a)
        {
            myText.text = "";
        }
        else
        {
            myText.text = a;
        }
    }
}
