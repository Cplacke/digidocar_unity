﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Button5Script : MonoBehaviour {

    public Button button;
    public GameObject scrollview;

	// Use this for initialization
	void Start () {
        Button myButton = button.GetComponent<Button>();
        myButton.onClick.AddListener(TaskOnClick);
        scrollview.SetActive(false);

    }

    private void TaskOnClick()
    {
        if (scrollview.activeInHierarchy == false)
        {
            scrollview.SetActive(true);
        }
        else
        {
            scrollview.SetActive(false);
        }

    }

}