﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Toggle1Script : MonoBehaviour {

    public Toggle toggle1;
    public GameObject sprite1;

	// Use this for initialization
	void Start () {
        Toggle toggle = toggle1.GetComponent<Toggle>();
        toggle.onValueChanged.AddListener(TaskOnChange);
        sprite1.SetActive(false);
    }
	
    void TaskOnChange(bool value)
    {
        if (toggle1.isOn)
        {
            sprite1.SetActive(true);
        }
        else
        {
            sprite1.SetActive(false);
        }
    }
}
